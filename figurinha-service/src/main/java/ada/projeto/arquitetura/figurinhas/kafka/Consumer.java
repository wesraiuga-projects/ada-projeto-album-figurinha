package ada.projeto.arquitetura.figurinhas.kafka;

import ada.projeto.arquitetura.figurinhas.service.FigurinhaService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class Consumer {
    
    private final static String ALBUM_CRIADO_TOPIC = "ALBUM_CRIADO";
    
    private FigurinhaService service;
    
    @KafkaListener(topics = ALBUM_CRIADO_TOPIC, groupId = "figurinha_group")
    public void consomeAlbumCriado(ConsumerRecord<String, String> payload) {
        // ALBUM_CRIADO
        // TODO: Criar figurinhas com base no protótipo do álbum 
        //  e nas quantidades de cada protótipo de figurinha
    }
}
