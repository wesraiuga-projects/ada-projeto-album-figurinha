package ada.projeto.arquitetura.figurinhas.model.dto;

import ada.projeto.arquitetura.figurinhas.model.entity.Raridade;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PrototipoFigurinhaDTO {

    private Integer id;

    private Integer prototipoAlbumId;

    private String nome;

    private String descricao;

    private Raridade raridade;

    private String imagem;
    
}
