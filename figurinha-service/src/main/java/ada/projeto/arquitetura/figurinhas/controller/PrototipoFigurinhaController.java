package ada.projeto.arquitetura.figurinhas.controller;
import ada.projeto.arquitetura.figurinhas.model.dto.PrototipoFigurinhaDTO;
import ada.projeto.arquitetura.figurinhas.service.PrototipoFigurinhaService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/figurinhas/prototipos")
public class PrototipoFigurinhaController {

    @Autowired
    private PrototipoFigurinhaService service;

    @PostMapping
    public ResponseEntity<Object> criar(@RequestBody PrototipoFigurinhaDTO dto) {
        try {
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(service.criar(dto));

        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(ex.getMessage());
        }
    }

    @PostMapping("batch")
    public ResponseEntity<Object> criarVarios(@RequestBody List<PrototipoFigurinhaDTO> listDTO) {
        try {
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(service.criarVarios(listDTO));

        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(ex.getMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> editar(
            @RequestBody PrototipoFigurinhaDTO dto,
            @PathVariable("id") Integer id) {

        try {
            return ResponseEntity.ok(service.editar(dto, id));

        } catch (EntityNotFoundException ex) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(ex.getMessage());

        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(ex.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deletar(@PathVariable("id") Integer id) {

        try {
            service.deletar(id);
            return ResponseEntity
                    .ok("Protótipo de Figurinha com id " + id + " removido com sucesso!");

        } catch (EntityNotFoundException ex) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(ex.getMessage());

        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(ex.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> pegarPorId(@PathVariable("id") Integer id) {

        try {
            return ResponseEntity.ok(service.pegarPorId(id));

        } catch (EntityNotFoundException ex) {
            return ResponseEntity
                    .status(HttpStatus.NO_CONTENT)
                    .body(ex.getMessage());

        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(ex.getMessage());
        }
    }

    @GetMapping
    public ResponseEntity<Object> listar() {

        try {
            return ResponseEntity.ok(service.listar());

        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(ex.getMessage());
        }
    }

    @GetMapping("/album/{id}")
    public ResponseEntity<Object> listarPorPrototipoAlbum(@PathVariable("id") Integer id) {

        try {
            return ResponseEntity.ok(service.listarPorPrototipoAlbum(id));

        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(ex.getMessage());
        }
    }

}
