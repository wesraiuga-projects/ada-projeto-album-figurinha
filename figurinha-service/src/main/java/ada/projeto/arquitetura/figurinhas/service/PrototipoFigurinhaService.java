package ada.projeto.arquitetura.figurinhas.service;

import ada.projeto.arquitetura.figurinhas.model.dto.PrototipoFigurinhaDTO;
import ada.projeto.arquitetura.figurinhas.model.entity.PrototipoFigurinhaEntity;
import ada.projeto.arquitetura.figurinhas.model.mapper.PrototipoFigurinhaMapper;
import ada.projeto.arquitetura.figurinhas.repository.PrototipoFigurinhaRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PrototipoFigurinhaService {

    @Autowired
    private PrototipoFigurinhaRepository repository;

    @Autowired
    private PrototipoFigurinhaMapper mapper;

    public PrototipoFigurinhaDTO criar(PrototipoFigurinhaDTO dto) {
        PrototipoFigurinhaEntity entity = mapper.toEntity(dto);

        entity = repository.save(entity);

        return mapper.toDTO(entity);
    }

    public List<PrototipoFigurinhaDTO> criarVarios(List<PrototipoFigurinhaDTO> listDTO) {
        List<PrototipoFigurinhaEntity> listEntity = mapper.toListEntity(listDTO);

        listEntity = repository.saveAll(listEntity);

        return mapper.toListDTO(listEntity);
    }

    public PrototipoFigurinhaDTO editar(PrototipoFigurinhaDTO dto, Integer id) {
        if (repository.existsById(id)) {
            PrototipoFigurinhaEntity entity = mapper.toEntity(dto);
            entity.setId(id);
            entity = repository.save(entity);

            return mapper.toDTO(entity);
        }

        throw new EntityNotFoundException("Protótipo de Figurinha não encontrado");
    }

    public void deletar(Integer id){
        Optional<PrototipoFigurinhaEntity> entityOp = repository.findById(id);

        if (entityOp.isPresent()) {
            PrototipoFigurinhaEntity entity = entityOp.get();
            repository.delete(entity);
            return;
        }

        throw new EntityNotFoundException("Protótipo de Figurinha não encontrado");
    }

    public PrototipoFigurinhaDTO pegarPorId(Integer id) {
        Optional<PrototipoFigurinhaEntity> entityOp = repository.findById(id);

        if (entityOp.isPresent()) {
            PrototipoFigurinhaEntity entity = entityOp.get();
            return mapper.toDTO(entity);
        }

        throw new EntityNotFoundException("Protótipo de Figurinha não encontrado");
    }

    public List<PrototipoFigurinhaDTO> listar() {
        List<PrototipoFigurinhaEntity> listaEntities = repository.findAll();
        return mapper.toListDTO(listaEntities);
    }

    @Cacheable(value = "prototiposFigurinhas_prototipoAlbum_cache")
    public List<PrototipoFigurinhaDTO> listarPorPrototipoAlbum(Integer albumId) {
        List<PrototipoFigurinhaEntity> listaEntities = repository.findByPrototipoAlbumId(albumId);
        return mapper.toListDTO(listaEntities);
    }
}
