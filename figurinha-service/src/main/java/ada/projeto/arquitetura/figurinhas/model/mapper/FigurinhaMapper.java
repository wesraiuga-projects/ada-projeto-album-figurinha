package ada.projeto.arquitetura.figurinhas.model.mapper;

import ada.projeto.arquitetura.figurinhas.model.dto.FigurinhaDTO;
import ada.projeto.arquitetura.figurinhas.model.dto.PrototipoFigurinhaDTO;
import ada.projeto.arquitetura.figurinhas.model.entity.FigurinhaEntity;
import ada.projeto.arquitetura.figurinhas.service.PrototipoFigurinhaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FigurinhaMapper {

    @Autowired
    private PrototipoFigurinhaService prototipoService;

    @Autowired
    private PrototipoFigurinhaMapper prototipoMapper;
    
    public FigurinhaDTO toDTO(FigurinhaEntity entity) {

        FigurinhaDTO dto = new FigurinhaDTO();
        dto.setId(entity.getId());
        dto.setAlbumId(entity.getAlbumId());
        dto.setPrototipoFigurinhaId(entity.getPrototipoFigurinha().getId());
        
        return dto;
    }

    public FigurinhaEntity toEntity(FigurinhaDTO dto) {

        FigurinhaEntity entity = new FigurinhaEntity();
        entity.setId(dto.getId());
        entity.setAlbumId(dto.getAlbumId());
        
        PrototipoFigurinhaDTO prototipoDTO = prototipoService.pegarPorId(dto.getPrototipoFigurinhaId());
        entity.setPrototipoFigurinha(prototipoMapper.toEntity(prototipoDTO));

        return entity;
    }

    public List<FigurinhaEntity> toListEntity(List<FigurinhaDTO> listaDTO){
        return listaDTO.stream()
                .map(this::toEntity)
                .toList();
    }

    public List<FigurinhaDTO> toListDTO(List<FigurinhaEntity> listaDTO){
        return listaDTO.stream()
                .map(this::toDTO)
                .toList();
    }

}
