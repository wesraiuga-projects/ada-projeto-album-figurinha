package ada.projeto.arquitetura.figurinhas.repository;

import ada.projeto.arquitetura.figurinhas.model.entity.PrototipoFigurinhaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PrototipoFigurinhaRepository extends JpaRepository<PrototipoFigurinhaEntity,Integer> {
    
    List<PrototipoFigurinhaEntity> findByPrototipoAlbumId(Integer albumId);
    
}
