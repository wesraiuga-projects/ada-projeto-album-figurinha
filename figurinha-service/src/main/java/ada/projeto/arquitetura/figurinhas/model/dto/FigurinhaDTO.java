package ada.projeto.arquitetura.figurinhas.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FigurinhaDTO {

    private Integer id;

    private Integer prototipoFigurinhaId;

    private Integer albumId;

}
