package ada.projeto.arquitetura.figurinhas.model.mapper;

import ada.projeto.arquitetura.figurinhas.model.dto.PrototipoFigurinhaDTO;
import ada.projeto.arquitetura.figurinhas.model.entity.PrototipoFigurinhaEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PrototipoFigurinhaMapper {

    public PrototipoFigurinhaDTO toDTO(PrototipoFigurinhaEntity entity) {

        PrototipoFigurinhaDTO dto = new PrototipoFigurinhaDTO();
        dto.setId(entity.getId());
        dto.setPrototipoAlbumId(entity.getPrototipoAlbumId());
        dto.setNome(entity.getNome());
        dto.setDescricao(entity.getDescricao());
        dto.setRaridade(entity.getRaridade());
        dto.setImagem(entity.getImagem());
        
        return dto;
    }

    public PrototipoFigurinhaEntity toEntity(PrototipoFigurinhaDTO dto) {

        PrototipoFigurinhaEntity entity = new PrototipoFigurinhaEntity();
        entity.setId(dto.getId());
        entity.setPrototipoAlbumId(dto.getPrototipoAlbumId());
        entity.setNome(dto.getNome());
        entity.setDescricao(dto.getDescricao());
        entity.setRaridade(dto.getRaridade());
        entity.setImagem(dto.getImagem());

        return entity;
    }

    public List<PrototipoFigurinhaEntity> toListEntity(List<PrototipoFigurinhaDTO> listDTO){
        return listDTO.stream()
                .map(this::toEntity)
                .toList();
    }

    public List<PrototipoFigurinhaDTO> toListDTO(List<PrototipoFigurinhaEntity> listDTO){
        return listDTO.stream()
                .map(this::toDTO)
                .toList();
    }

}
