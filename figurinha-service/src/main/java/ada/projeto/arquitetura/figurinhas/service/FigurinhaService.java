package ada.projeto.arquitetura.figurinhas.service;

import ada.projeto.arquitetura.figurinhas.model.dto.FigurinhaDTO;
import ada.projeto.arquitetura.figurinhas.model.dto.PrototipoFigurinhaDTO;
import ada.projeto.arquitetura.figurinhas.model.entity.FigurinhaEntity;
import ada.projeto.arquitetura.figurinhas.model.mapper.FigurinhaMapper;
import ada.projeto.arquitetura.figurinhas.model.mapper.PrototipoFigurinhaMapper;
import ada.projeto.arquitetura.figurinhas.repository.FigurinhaRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FigurinhaService {

    @Autowired
    private FigurinhaRepository repository;

    @Autowired
    private FigurinhaMapper mapper;

    public FigurinhaDTO criar(FigurinhaDTO dto) {        
        FigurinhaEntity entity = mapper.toEntity(dto);

        entity = repository.save(entity);

        return mapper.toDTO(entity);
    }

    public List<FigurinhaDTO> criarVarios(List<FigurinhaDTO> listDTO) {
        List<FigurinhaEntity> listEntity = mapper.toListEntity(listDTO);
        
        listEntity = repository.saveAll(listEntity);

        return mapper.toListDTO(listEntity);
    }

    public FigurinhaDTO editar(FigurinhaDTO dto, Integer id) {
        if (repository.existsById(id)) {
            FigurinhaEntity entity = mapper.toEntity(dto);
            entity.setId(id);
            entity = repository.save(entity);

            return mapper.toDTO(entity);
        }

        throw new EntityNotFoundException("Figurinha não encontrada");
    }

    public void deletar(Integer id){
        Optional<FigurinhaEntity> entityOp = repository.findById(id);

        if (entityOp.isPresent()) {
            FigurinhaEntity entity = entityOp.get();
            repository.delete(entity);
            return;
        }

        throw new EntityNotFoundException("Figurinha não encontrada");
    }

    public FigurinhaDTO pegarPorId(Integer id) {
        Optional<FigurinhaEntity> entityOp = repository.findById(id);

        if (entityOp.isPresent()) {
            FigurinhaEntity entity = entityOp.get();
            return mapper.toDTO(entity);
        }

        throw new EntityNotFoundException("Figurinha não encontrada");
    }

    public List<FigurinhaDTO> listar() {
        List<FigurinhaEntity> listaEntities =  repository.findAll();
        return mapper.toListDTO(listaEntities);
    }

//    @Cacheable(value = "figurinhas_album_cache")
    public List<FigurinhaDTO> listarPorAlbum(Integer albumId) {
        List<FigurinhaEntity> listaEntities =  repository.findByAlbumId(albumId);
        return mapper.toListDTO(listaEntities);
    }
    
//    @Cacheable(value = "figurinhas_prototipo_cache")
    public List<FigurinhaDTO> listarPorPrototipo(Integer prototipoFigurinhaId) {
        List<FigurinhaEntity> listaEntities =  repository.findByPrototipoFigurinhaId(prototipoFigurinhaId);
        return mapper.toListDTO(listaEntities);
    }
    
}
