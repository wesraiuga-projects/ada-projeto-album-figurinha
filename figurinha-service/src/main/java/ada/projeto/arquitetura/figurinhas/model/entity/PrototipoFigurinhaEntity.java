package ada.projeto.arquitetura.figurinhas.model.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name="prototipo_figurinha")
@ToString
public class PrototipoFigurinhaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="id_prototipo_album", nullable=false)
    private Integer prototipoAlbumId;

    @Column(name="nome", nullable=false, unique = true)
    private String nome;

    @Column(name="descricao", nullable=false)
    private String descricao;

    @Column(name="raridade", nullable=false)
    private Raridade raridade;

    @Column(name="imagem", nullable=false)
    private String imagem;

    @OneToMany(mappedBy = "prototipoFigurinha", cascade = CascadeType.REMOVE)
    private List<FigurinhaEntity> figurinhas;
    
}
