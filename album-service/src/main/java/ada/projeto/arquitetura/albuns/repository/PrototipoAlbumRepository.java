package ada.projeto.arquitetura.albuns.repository;

import ada.projeto.arquitetura.albuns.model.entity.PrototipoAlbumEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrototipoAlbumRepository extends JpaRepository<PrototipoAlbumEntity,Integer> {
        
}
