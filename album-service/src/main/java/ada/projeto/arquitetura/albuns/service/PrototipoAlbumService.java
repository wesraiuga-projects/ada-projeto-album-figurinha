package ada.projeto.arquitetura.albuns.service;

import ada.projeto.arquitetura.albuns.model.dto.PrototipoAlbumDTO;
import ada.projeto.arquitetura.albuns.model.entity.PrototipoAlbumEntity;
import ada.projeto.arquitetura.albuns.model.mapper.PrototipoAlbumMapper;
import ada.projeto.arquitetura.albuns.repository.PrototipoAlbumRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PrototipoAlbumService {

    @Autowired
    private PrototipoAlbumRepository repository;

    @Autowired
    private PrototipoAlbumMapper mapper;

    public PrototipoAlbumDTO criar(PrototipoAlbumDTO dto) {
        PrototipoAlbumEntity entity = mapper.toEntity(dto);

        entity = repository.save(entity);

        return mapper.toDTO(entity);
    }

    public List<PrototipoAlbumDTO> criarVarios(List<PrototipoAlbumDTO> listDTO) {
        List<PrototipoAlbumEntity> listEntity = mapper.toListEntity(listDTO);

        listEntity = repository.saveAll(listEntity);

        return mapper.toListDTO(listEntity);
    }

    public PrototipoAlbumDTO editar(PrototipoAlbumDTO dto, Integer id) {
        if (repository.existsById(id)) {
            PrototipoAlbumEntity entity = mapper.toEntity(dto);
            entity.setId(id);
            entity = repository.save(entity);

            return mapper.toDTO(entity);
        }

        throw new EntityNotFoundException("Protótipo de Álbum não encontrado");
    }

    public void deletar(Integer id){
        Optional<PrototipoAlbumEntity> entityOp = repository.findById(id);

        if (entityOp.isPresent()) {
            PrototipoAlbumEntity entity = entityOp.get();
            repository.delete(entity);
            return;
        }

        throw new EntityNotFoundException("Protótipo de Álbum não encontrado");
    }

    public PrototipoAlbumDTO pegarPorId(Integer id) {
        Optional<PrototipoAlbumEntity> entityOp = repository.findById(id);

        if (entityOp.isPresent()) {
            PrototipoAlbumEntity entity = entityOp.get();
            return mapper.toDTO(entity);
        }

        throw new EntityNotFoundException("Protótipo de Álbum não encontrado");
    }

    public List<PrototipoAlbumDTO> listar() {
        List<PrototipoAlbumEntity> listaEntities = repository.findAll();
        return mapper.toListDTO(listaEntities);
    }
    
}
