package ada.projeto.arquitetura.albuns.model.mapper;

import ada.projeto.arquitetura.albuns.model.dto.AlbumDTO;
import ada.projeto.arquitetura.albuns.model.dto.PrototipoAlbumDTO;
import ada.projeto.arquitetura.albuns.model.entity.AlbumEntity;
import ada.projeto.arquitetura.albuns.service.PrototipoAlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AlbumMapper {

    @Autowired
    private PrototipoAlbumService prototipoService;

    @Autowired
    private PrototipoAlbumMapper prototipoMapper;
    
    public AlbumDTO toDTO(AlbumEntity entity) {

        AlbumDTO dto = new AlbumDTO();
        dto.setId(entity.getId());
        dto.setPrototipoAlbumId(entity.getPrototipoAlbum().getId());
        dto.setUsuarioId(entity.getUsuarioId());
        
        return dto;
    }

    public AlbumEntity toEntity(AlbumDTO dto) {

        AlbumEntity entity = new AlbumEntity();
        entity.setId(dto.getId());
        entity.setUsuarioId(dto.getUsuarioId());
        
        PrototipoAlbumDTO prototipoDTO = prototipoService.pegarPorId(dto.getPrototipoAlbumId());
        entity.setPrototipoAlbum(prototipoMapper.toEntity(prototipoDTO));

        return entity;
    }

    public List<AlbumEntity> toListEntity(List<AlbumDTO> listaDTO){
        return listaDTO.stream()
                .map(this::toEntity)
                .toList();
    }

    public List<AlbumDTO> toListDTO(List<AlbumEntity> listaDTO){
        return listaDTO.stream()
                .map(this::toDTO)
                .toList();
    }

}
