package ada.projeto.arquitetura.albuns.service;

import ada.projeto.arquitetura.albuns.kafka.Producer;
import ada.projeto.arquitetura.albuns.model.dto.AlbumDTO;
import ada.projeto.arquitetura.albuns.model.entity.AlbumEntity;
import ada.projeto.arquitetura.albuns.model.mapper.AlbumMapper;
import ada.projeto.arquitetura.albuns.repository.AlbumRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AlbumService {
    
    private final static String ALBUM_CRIADO_TOPIC = "ALBUM_CRIADO";

    @Autowired
    private AlbumRepository repository;

    @Autowired
    private AlbumMapper mapper;

    @Autowired
    private Producer producer;
    
    @Autowired
    ObjectMapper jsonMapper;

    public AlbumDTO criar(AlbumDTO dto) throws JsonProcessingException {        
        AlbumEntity entity = mapper.toEntity(dto);

        entity = repository.save(entity);

        String payload = jsonMapper.writeValueAsString(mapper.toDTO(entity));
        System.out.println(payload);
        producer.send(ALBUM_CRIADO_TOPIC, payload);

        return mapper.toDTO(entity);
    }

    public List<AlbumDTO> criarVarios(List<AlbumDTO> listDTO) {
        List<AlbumEntity> listEntity = mapper.toListEntity(listDTO);
        
        listEntity = repository.saveAll(listEntity);

        return mapper.toListDTO(listEntity);
    }

    public AlbumDTO editar(AlbumDTO dto, Integer id) {
        if (repository.existsById(id)) {
            AlbumEntity entity = mapper.toEntity(dto);
            entity.setId(id);
            entity = repository.save(entity);

            return mapper.toDTO(entity);
        }

        throw new EntityNotFoundException("Álbum não encontrado");
    }

    public void deletar(Integer id){
        Optional<AlbumEntity> entityOp = repository.findById(id);

        if (entityOp.isPresent()) {
            AlbumEntity entity = entityOp.get();
            repository.delete(entity);
            return;
        }

        throw new EntityNotFoundException("Álbum não encontrado");
    }

    public AlbumDTO pegarPorId(Integer id) {
        Optional<AlbumEntity> entityOp = repository.findById(id);

        if (entityOp.isPresent()) {
            AlbumEntity entity = entityOp.get();
            return mapper.toDTO(entity);
        }

        throw new EntityNotFoundException("Álbum não encontrado");
    }

    public List<AlbumDTO> listar() {
        List<AlbumEntity> listaEntities =  repository.findAll();
        return mapper.toListDTO(listaEntities);
    }

    //    @Cacheable(value = "album_prototipo_cache")
    public Object listarPorPrototipo(Integer prototipoAlbumId) {
        List<AlbumEntity> listaEntities =  repository.findByPrototipoAlbumId(prototipoAlbumId);
        return mapper.toListDTO(listaEntities);
    }
    
//    @Cacheable(value = "album_usuario_cache")
    public Object listarPorUsuario(Integer usuarioId) {
        List<AlbumEntity> listaEntities =  repository.findByUsuarioId(usuarioId);
        return mapper.toListDTO(listaEntities);
    }
    
}
