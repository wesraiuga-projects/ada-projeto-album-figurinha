package ada.projeto.arquitetura.albuns.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;

public class Consumer {
    
    @KafkaListener(topics = "ALBUM_VENDIDO", groupId = "album_group")
    public void consomeAlbumVendido(ConsumerRecord<String, String> payload) {
        // ALBUM_CRIADO
    }
}
