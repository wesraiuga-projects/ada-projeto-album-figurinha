package ada.projeto.arquitetura.albuns.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PrototipoAlbumDTO {

    private Integer id;

    private String nome;

    private String descricao;
    
}
