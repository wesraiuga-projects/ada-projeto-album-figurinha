package ada.projeto.arquitetura.albuns.repository;

import ada.projeto.arquitetura.albuns.model.entity.AlbumEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AlbumRepository extends JpaRepository<AlbumEntity,Integer> {

    List<AlbumEntity> findByPrototipoAlbumId(Integer albumId);
    List<AlbumEntity> findByUsuarioId(Integer albumId);
    
}
