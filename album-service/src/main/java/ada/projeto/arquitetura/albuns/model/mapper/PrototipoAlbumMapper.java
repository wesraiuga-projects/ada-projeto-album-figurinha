package ada.projeto.arquitetura.albuns.model.mapper;

import ada.projeto.arquitetura.albuns.model.dto.PrototipoAlbumDTO;
import ada.projeto.arquitetura.albuns.model.entity.PrototipoAlbumEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PrototipoAlbumMapper {

    public PrototipoAlbumDTO toDTO(PrototipoAlbumEntity entity) {

        PrototipoAlbumDTO dto = new PrototipoAlbumDTO();
        dto.setId(entity.getId());
        dto.setNome(entity.getNome());
        dto.setDescricao(entity.getDescricao());
        
        return dto;
    }

    public PrototipoAlbumEntity toEntity(PrototipoAlbumDTO dto) {

        PrototipoAlbumEntity entity = new PrototipoAlbumEntity();
        entity.setId(dto.getId());
        entity.setNome(dto.getNome());
        entity.setDescricao(dto.getDescricao());

        return entity;
    }

    public List<PrototipoAlbumEntity> toListEntity(List<PrototipoAlbumDTO> listDTO){
        return listDTO.stream()
                .map(this::toEntity)
                .toList();
    }

    public List<PrototipoAlbumDTO> toListDTO(List<PrototipoAlbumEntity> listDTO){
        return listDTO.stream()
                .map(this::toDTO)
                .toList();
    }

}
