package ada.projeto.arquitetura.albuns.model.entity;

public enum Raridade {
    EPICO,
    MUITO_RARO,
    RARO,
    COMUM;
}
