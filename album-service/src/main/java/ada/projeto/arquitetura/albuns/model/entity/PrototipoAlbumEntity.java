package ada.projeto.arquitetura.albuns.model.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name="prototipo_album")
@ToString
public class PrototipoAlbumEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="nome", nullable=false, unique = true)
    private String nome;

    @Column(name="descricao", nullable=false)
    private String descricao;

    @OneToMany(mappedBy = "prototipoAlbum", cascade = CascadeType.REMOVE)
    private List<AlbumEntity> albuns;
    
}
