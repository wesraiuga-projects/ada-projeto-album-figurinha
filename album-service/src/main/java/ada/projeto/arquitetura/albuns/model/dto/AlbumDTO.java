package ada.projeto.arquitetura.albuns.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AlbumDTO {

    private Integer id;

    private Integer prototipoAlbumId;

    private Integer usuarioId;

}
